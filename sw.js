self.addEventListener("install", (event) => {
  console.log("Service Worker installing.");
});

self.addEventListener("fetch", function (event) {
  console.log("Fetching:", event.request.url);
  // Here, you can intercept requests and respond with cached responses
});
